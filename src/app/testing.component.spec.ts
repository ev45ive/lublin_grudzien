import { By } from '@angular/platform-browser'
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      providers:[
        { provide: 'test', useValue: 'Testing' }
      ],
      imports:[]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render text', () => {
    let elem = fixture.debugElement.query(By.css('p'))
    expect(elem.nativeElement.textContent).toEqual('Testing');
  });

  it('should render message', () => {
    component.message = 'Testing changed'
    fixture.detectChanges()

    let elem = fixture.debugElement.query(By.css('p'))
    expect(elem.nativeElement.textContent).toEqual('Testing changed');
  });

  it('should render text', async(() => {
    fixture.detectChanges()
    fixture.whenStable().then(()=>{

      let elem = fixture.debugElement.query(By.css('p'))
      expect(elem.nativeElement.textContent).toEqual('Testing');
    })
  }));


  
});
