import { MusicModule } from './music/music.module'
import { FormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, NgModule } from '@angular/core'

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { ItemsListComponent } from './playlists/items-list.component';
import { ListItemComponent } from './playlists/list-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { PlaylistsService } from './playlists/playlists.service';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { Routing } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { ValidateColorDirective } from './playlists/validate-color.directive';
import { TestingComponent } from './testing.component';

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistContainerComponent,
    HighlightDirective,
    ValidateColorDirective,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MusicModule,
    AuthModule,
    Routing
  ],
  providers: [
    // // Override music.module providers
    // { 
    //   provide: 'search_url',
    //   useValue:'https://placki'
    // },
    { 
      provide: PlaylistsService,
      useClass: PlaylistsService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth:AuthService){
    this.auth.getToken()
  }
}
