import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs/Observable'
import { Component, OnInit, Inject } from '@angular/core';
import { Playlist } from './playlist';
import { PlaylistsService } from './playlists.service';
import { map, pluck, switchMap } from 'rxjs/operators'

@Component({
  selector: 'playlists',
  templateUrl: './playlists.component.html',
  styles: []
})
export class PlaylistsComponent implements OnInit {

  playlists:Observable<Playlist[]>

  selected: Observable<Playlist>

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService:PlaylistsService
  ) {
    
  }

  select(playlist:Playlist){
    this.router
        .navigate(['/playlists',playlist.id])

        // .navigate([playlist.id],{
        //   relativeTo: this.route
        // })
  }
  
  ngOnInit() {
  
    // get all playlists
    this.playlists = this.playlistsService
        .getPlaylists()
    
    // get selected from url param 
    this.selected = this.route.params.pipe(
      pluck<any,number>('playlist_id'),
      switchMap(id => this.playlistsService.getPlaylist(id))
    )
  }

}
