import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core'
import { Playlist } from './playlist';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'playlist-details',
  templateUrl: './playlist-details.component.html',
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {
  
  @Input()
  playlist:Playlist

  @ViewChild('playlistForm')
  form:NgForm

  mode = 'show'

  edit(){
    this.mode = 'edit'
    setTimeout(()=>{
      this.form.form.patchValue(this.playlist)

      // this.form.form.setValidators([])
    })
  }

  cancel(){
    this.mode = 'show'
  }

  @Output()
  saved = new EventEmitter<Playlist>()

  save(form:NgForm){
    if(form.invalid){
      return
    }

    let playlist:Playlist = {
      ...this.playlist,
      ...form.value
    }
    this.saved.emit(playlist)
  }

  constructor() { 

  }
  
  ngOnInit() {
  }
  
  ngAfterViewInit(){
    // console.log(this.form)
  }

}
