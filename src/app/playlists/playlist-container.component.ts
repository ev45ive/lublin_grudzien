import { ActivatedRoute } from '@angular/router'
import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { map, switchMap } from 'rxjs/operators'

@Component({
  selector: 'playlist-container',
  templateUrl: './playlist-container.component.html',
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService
  ) {

    this.selected = route
      .params
      .pipe(
        map(params => parseInt(params['playlist_id'], 10)),
        switchMap(id => this.playlistsService
            .getPlaylist(id))
      )

  }

  selected

  ngOnInit() {
  }

}
