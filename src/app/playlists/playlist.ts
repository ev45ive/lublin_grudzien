/**
  Playlist interface
 */
export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
    Playlist tracks
    @optional 
   */
  tracks?: Array<Track>
}

// class Playlist(){
//   constructor(public id:number){

//   }

//  get name(){ return 'X!'}
// }

interface Track {

}

// function find<T>(arr:Array<T>){
//   return arr[0]
// }


// var test:string;

// test = find([1,2,3])

// test = find(['x','y'])