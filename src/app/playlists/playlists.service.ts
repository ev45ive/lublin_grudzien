import { Injectable } from '@angular/core';
import { Playlist } from './playlist';
import { of } from 'rxjs/observable/of'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PlaylistsService {

  playlists: Playlist[] = [
    {
      "id": 1,
      "name": "Angular Greatest HITS!",
      "favourite": true,
      "color": "#ff0000"
    },
    {
      "id": 2,
      "name": "Angular Top20",
      "favourite": false,
      "color": "#00ff00"
    },
    {
      "id": 3,
      "name": "The best of Angular",
      "favourite": true,
      "color": "#00ff"
    }
  ]

  constructor() { }

  getPlaylists(){
    // this.http.get('playlists...
    return of(this.playlists)
  }

  getPlaylist(id){
    return of(this.playlists.find(
      playlist => playlist.id == id
    ))
  }

  savePlaylist(playlist){
    return of(playlist)
  }
}
