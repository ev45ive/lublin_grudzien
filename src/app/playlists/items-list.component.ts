import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core'
import { Playlist } from './playlist';

@Component({
  selector: 'items-list',
  templateUrl: './items-list.component.html',
  styles: [`
    .list-group-item{
      border-left:10px solid black;
    }
    :host(.bordered){
      border:2px solid black;
      display: block;
    }
    :host(.colored) ::ng-deep p{
      color: hotpink;
    }
    :host-context(.error){
      color: red !important;
    }
  `],
  // inputs:['items:playlists']
  // encapsulation: ViewEncapsulation.Emulated
})
export class ItemsListComponent implements OnInit {

  @Input()
  playlists: Playlist[]

  @Input()
  selected:Playlist

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist:Playlist){
    this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit() {
  }

}
