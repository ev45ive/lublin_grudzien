import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core'

@Directive({
  selector: '[highlight], .highlight',
  // host:{
  //   '(mouseenter)':'onEnter($event)'
  // }
})
export class HighlightDirective implements OnInit{

  @Input('highlight')
  set highlight(color){
    this.color = color;
    this.activeColor = this.hover? this.color : 'inherit'
  }
  color

  @HostBinding('style.color')
  activeColor

  hover = false

  @HostListener('mouseenter',['$event.x'])
  onEnter(x:number){
    this.hover = true
    this.activeColor = this.color
  }

  @HostListener('mouseleave')
  onLeave(){
    this.hover = false
    this.activeColor =  'inherit'
  }

  constructor(
    private elem: ElementRef,
    private renderer:Renderer2
  ) {

    // this.elem.nativeElement.style.color = 'red'
    // $(this.elem.nativeElement)
    
    // console.log('hello', this.elem)
  }
  


  ngOnInit(){
    this.renderer.setStyle(this.elem.nativeElement,'color',this.color)
  }

  ngOnDestroy(){

  }

}
