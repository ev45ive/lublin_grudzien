import { Directive, Input} from '@angular/core';
import { NgModel, NG_VALIDATORS, NG_ASYNC_VALIDATORS, Validator, AsyncValidator } from '@angular/forms';

@Directive({
  selector: '[validateColor]',
  providers:[
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateColorDirective,
      multi: true
    }
  ]
})
export class ValidateColorDirective implements Validator {

  @Input('validateColor')
  params

  constructor(){ //private model:NgModel) {
    // console.log(this)

    // this.model.control.setValidators([
    //   this.validate
    // ])
  }

  validate(control) {
    let value = control.value
    let isError = value && value.indexOf('#') == -1
    return isError ? {'color':true} : null
  }

}
