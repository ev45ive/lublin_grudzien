import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { MusicSearchService } from './music-search.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent, 
    AlbumListComponent, 
    AlbumItemComponent
  ],
  providers:[
    { 
      provide: 'search_url',
      useValue:'https://api.spotify.com/v1/search'
    },
    // {
    //   provide: MusicSearchService,
    //   useFactory: function(url){
    //     return new MusicSearchService(url)
    //   }, deps: [ 'search_url']
    // }
    // {provide: MusicSearchService, useClass:
    // SpecialChristmassMusicSearchService }
    // { provide: MusicSearchService, useClass: MusicSearchService}
    MusicSearchService
  ],
  exports:[
    MusicSearchComponent
  ]
})
export class MusicModule { }
