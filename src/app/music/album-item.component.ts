import { Component, Input, OnInit } from '@angular/core'
import { Album } from './album';

@Component({
  selector: 'album-item',
  templateUrl: './album-item.component.html',
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input()
  album:Album

  constructor() { }

  ngOnInit() {
  }

}
