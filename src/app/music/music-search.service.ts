import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Inject, Injectable } from '@angular/core'
import { Album } from './album';
import { AuthService } from '../auth/auth.service';
import { catchError, map, pluck, startWith } from 'rxjs/operators'
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// import 'rxjs/Rx'
// import 'rxjs/add/operator/pluck'

interface APIResponse<Name,T> {
  [Name: string]: PaginationObject<T>
}
interface PaginationObject<T> {
  items: T[];
  total: number;
  limit: number;
}

type AlbumsResponse = APIResponse<'albums', Album>

@Injectable()
export class MusicSearchService {

  // albums$ = new EventEmitter<Album[]>()
  albums$ = new BehaviorSubject<Album[]>([])
  
  constructor(
    @Inject('search_url')
    private search_url,
    private http: HttpClient
  ) {
    console.log(this.albums$)
  }

  search(query){
    this.http.get<AlbumsResponse>(this.search_url, {
      params: {
        type: 'album',
        q: query
      }
    })    
    .pipe(
      // pluck<any,Album[]>('albums','items')
      map(response => response.albums.items),
    )
    .subscribe(albums =>{
      this.albums$.next(albums)
    })
  }

  getAlbums(query = 'batman') {

    return this.albums$.asObservable()
  }

}
