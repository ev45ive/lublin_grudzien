import { tap } from 'rxjs/operators'
import { selector } from 'rxjs/operator/publish'
import { Album } from './album'
import { MusicSearchService } from './music-search.service'
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'albums-list',
  templateUrl: './albums-list.component.html',
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;
    }
  `]
})
export class AlbumListComponent implements OnInit {

  albums$:Observable<Album[]>

  constructor(
    private musicService:MusicSearchService
  ) { 

    this.albums$ = this.musicService
      .getAlbums()
  }

  ngOnInit() {

  }
}
