import { MusicSearchService } from './music-search.service'
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'music-search',
  templateUrl: './music-search.component.html',
  styles: []
})
export class MusicSearchComponent implements OnInit {

  constructor(
    private musicService:MusicSearchService
  ) { }

  ngOnInit() {
  }

  search(query){
    this.musicService.search(query)
  }
}
