import { Component, OnInit, Output } from '@angular/core'
import {
  FormGroup, AbstractControl,
  FormControl, FormArray, Validators, ValidatorFn, AsyncValidatorFn, Validator, ValidationErrors
} from '@angular/forms'
import { Observable } from 'rxjs/Observable';
import { combineLatest, debounceTime, distinctUntilChanged, filter, withLatestFrom } from 'rxjs/operators'

// import 'rxjs/add/operator/map'

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styles: [`
    form .ng-dirty.ng-invalid,
    form .ng-touched.ng-invalid {
      border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  @Output()
  queryChange

  constructor() {

    const censor = (badword): ValidatorFn => {
      return (control: AbstractControl): ValidationErrors => {

        let hasError = control.value.indexOf(badword) !== -1;

        return hasError ? {
          'censor': badword
        } : null
      }
    }

    const asyncCensor = (badword): AsyncValidatorFn => {
      return (control: AbstractControl) => {

        // consturtor(private http:HttpClient)

        // validate(control){}
        // return this.http.get(....).map(response => ValidationErrors)

        return Observable.create(observable => {

          setTimeout(() => {
            observable.next(censor(badword)(control))
            observable.complete()
          }, 2000)

        })

      }
    }



    this.queryForm = new FormGroup({
      'query': new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        censor('batman')
      ], [
          asyncCensor('superman')
        ])
    })
    let queryField = this.queryForm.get('query')

    // Emits when VALID
    let valid$ = queryField.statusChanges.pipe(
      filter(status => status == "VALID")
    )

    // Emits when changed
    let value$ = queryField.valueChanges

    // Emits changes only when valid
    this.queryChange = valid$
      .pipe(
        debounceTime(400),
        // Only when valid emits, take latest value
        withLatestFrom(value$, (valid, value) => value),
        // combineLatest(value$,(valid,value)=>value),
        filter(query => query.length >= 3),
        distinctUntilChanged()
      )

    // reset in user event
    // this.queryForm.reset()

    window['form'] = this.queryForm
  }

  ngOnInit() {
  }

}
