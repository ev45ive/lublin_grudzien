import { catchError } from 'rxjs/operators'
import { AuthService } from './auth.service'
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(
    private auth: AuthService
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ) {

    let headers = req
      .headers
      .append('Authorization', 'Bearer ' + this.auth.getToken())

    let authorized = req.clone({
      headers
    })

    return next.handle(authorized).pipe(
      catchError(err => {

      if (
        err instanceof HttpErrorResponse && err.statusText == "Unauthorized"
      ) {
        this.auth.authenticate()          
      }else{
        throw err;
      }

        return []
      })
    )
  }

}
