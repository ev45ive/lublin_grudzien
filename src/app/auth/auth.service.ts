import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }

  authenticate(){
    let url = 'https://accounts.spotify.com/authorize/',
        response_type = 'token',
        client_id = '8fe48ff2c3f84e4b802e8706a701d540',
        redirect_uri = 'http://localhost:4200/'
    
    let redirect = `${url}?response_type=${response_type}&client_id=${client_id}&redirect_uri=${redirect_uri}`

    sessionStorage.removeItem('token')
    // Redirect to spotify:
    window.location.replace(redirect)
  }

  token

  getToken(){
    this.token = JSON.parse(sessionStorage.getItem('token'))

    if(!this.token){
      let match = window.location.hash.match(/access_token=([^&]*)/)
      this.token = match && match[1]
      window.location.hash = ''

      sessionStorage.setItem('token', JSON.stringify(this.token) )
    }
    
    if(!this.token){
      this.authenticate()
    }

    return this.token
  }

}
