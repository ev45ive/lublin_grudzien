import { Component, Inject, OnInit } from '@angular/core'

@Component({
  selector: 'testing',
  template:`
    <p>{{message}}</p>
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = 'Testing'

  constructor(
    @Inject('test') message
  ) { 
    this.message = message
  }

  ngOnInit() {
  }

}
