import { RouterModule, Routes } from '@angular/router'

import { PlaylistsComponent } from './playlists/playlists.component'
import { MusicSearchComponent } from './music/music-search.component';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';


const routes: Routes = [
  { path: '', redirectTo: 'playlists', pathMatch: 'full' },
  {
    path: 'playlists', children: [
      { 
        path: '', component: PlaylistsComponent
      },
      {
        path: ':playlist_id', component: PlaylistsComponent,
        children: [
          { path: '', component: PlaylistContainerComponent, pathMatch: 'full' },
          // { path: '/edit', component: PlaylistContainerComponent, pathMatch: 'full' },
        ]
      }
    ]
  },
  { path: 'music', component: MusicSearchComponent },
  { path: '**', redirectTo: 'playlists', pathMatch: 'full' },
]


export const Routing = RouterModule.forRoot(routes, {
  // enableTracing: true,
  // useHash: true,
  // errorHandler: ()={}
})